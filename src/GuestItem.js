import React, { Component } from 'react'


class GuestItem extends Component {
    render = () => {
        const { guest, changeStatus } = this.props;

        let { arrived, user } = guest;
        return(
            <li
                className={  arrived ? 'status__arrived' : 'status_notarrived' }
            >
                {user.name}
                <button onClick={ changeStatus(user.index) }>
                {
                    arrived ? 'Прибыл' : 'Отсутствует'
                }
                </button>
            </li>
        );
    }
}

export default GuestItem;