import React, { Component } from 'react'

import GuestItem from './GuestItem';

import guests from './guests.json';
import './App.css';

const converted_guests = guests.map( user => {
  return({
    user,
    arrived: false
  });
})

console.log( guests, converted_guests );

class App extends Component {


    constructor( props ){
        super( props );
        this.state = {
        guests: converted_guests,
        filtred_guests: [],
        value: ""
        }
    }

    changeUserStatus = ( index ) => ( event ) => {
        let changedUsers = this.state.guests.map( guest => {
            if( guest.user.index === index ){
            guest.arrived = !guest.arrived;
            }
            return guest;
        });

        this.setState({ guests: changedUsers });
    } 

    changeHandler = (e) => {
        const query = e.target.value.toLowerCase();
        const filtredUsers = this.state.guests.filter( guest => {
            let name = guest.user.name.toLowerCase();
            if( name.indexOf( query ) !== -1){
            return true;
            } else {
            return false;
            }

        })

        this.setState({
            value: query,
            filtred_guests: filtredUsers
        });

    }


    render(){

        const { changeUserStatus, changeHandler } = this;
        const { guests, filtred_guests, value } = this.state;

        let data = guests;
        if( filtred_guests.length > 0 ){
            data = filtred_guests;
        }

        return(
            <div>
                <h1> App </h1>
                <input value={value} onChange={changeHandler} />
                <ul>
                {
                    data.map( guest => {
                    return(
                        <GuestItem 
                        changeStatus={changeUserStatus}
                        guest={guest}
                        />
                    )
                    })
                }
                </ul>
            </div>
        );

    }

}

export default App;
